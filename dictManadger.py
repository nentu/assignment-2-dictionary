import subprocess
from subprocess import Popen, PIPE, STDOUT

class DictManadger():

    def __init__(self, sourceDict: dict):
        self._compile_main(sourceDict)

    def _compile_main(self, test: dict):
        i = 0
        file = open("words.inc", "w")
        i, res = self._dict_to_words_inc(test, i)
        file.write(res)
        file.close()
        subprocess.call("make main".split(),
                        stdout=subprocess.DEVNULL,
                        stderr=subprocess.STDOUT)
        
    def _dict_to_words_inc(self, test :dict, i: int) -> tuple:
        res = '%include "colon.inc"\n'
        for key_str in test.keys():
            res += """colon "{0}", {1}\ndb "{2}", 0\n""".format(key_str, "label"+str(i), test[key_str])
            i+=1
        res+="dict_end"
        return i, res
    
    def get(self, value: str) -> tuple:
        p = Popen("make -s run".split(), stdout=PIPE, stdin=PIPE, stderr=PIPE)
        value, strerr = [i.decode() for i in p.communicate(input=value.encode())]
        value = value.replace("Enter key: ", "")[:-1]
        return value, strerr
    
if __name__ == '__main__':
    dictM = DictManadger({
                "key1":"value\n\tvalue1"
            })