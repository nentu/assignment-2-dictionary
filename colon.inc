%define i 0 

%macro colon 2	;1- str key, link- value
item_%[i]: 
%assign i i+1 
dq item_%[i]  ;next lable
dq key_%[i]
%ifid %2
	dq %2
%else
	%error "Colon 2 must be label type"
%endif

%ifstr %1
	key_%[i]: dq %1, 0	;key
%else
	%error "Colon 1 must be string type"
%endif
%2:		;value
%endmacro 


%macro dict_end 0
item_%[i]: dq 0  ;end
dict_length: db i
%endmacro