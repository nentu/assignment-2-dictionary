.PHONY: test run
LIB = ./source/lib.o
DICT = ./source/dict.o
MAIN = ./source/main.o

NASM = nasm
NASMFLAGS = -f elf64 -o
LD = ld -o



./source/%.o: %.asm
	${NASM} ${NASMFLAGS} $@ $<

	
$(MAIN): main.asm words.inc lib.inc dict.inc colon.inc
	${NASM} ${NASMFLAGS} $@ $<
	
	
main: $(MAIN) $(LIB) $(DICT)
	ld -o $@ $^
	#./main

show_colon:
	${NASM} ${NASMFLAGS} -E words.inc -o ./source/words.o
	ld -o ./source/words ./source/words.o
	

#test:
#	nasm -f elf64 -o main.o main.res
#	ld -o mainres main.o ./source/lib.o
test: main
	python3 test.py
run:
	./main