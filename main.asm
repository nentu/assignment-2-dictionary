%define BUF_SIZE 255

section .bss
word_buf: resb BUF_SIZE;times 255 db 0xca

section .rodata
entry_text: db "Enter key: ", 0
not_found_text: db 'Key error: "', 0
key_length_text: db 'The key length must be less than 255.', 0
%include "words.inc"

section .text

%include "lib.inc"
%include "dict.inc"




print_value:
	mov rdi, item_0
	mov rsi, word_buf
    call find_word
	
	test rax, rax
	je .not_found
	.found: mov rdi, rax
	call print_string
	ret
	
    .not_found:
	mov rdi, not_found_text
	call print_error_string
	mov rdi, word_buf
	call print_error_string
	mov rdi, '"'
	call print_error_char
	ret
global _start
_start:
	mov rdi, entry_text
	call print_string
    mov rdi, word_buf
    mov rsi, BUF_SIZE 
    call read_string
	
	test rax, rax
	je .wrongKeyLength
	call print_value
	jmp .cont
	.wrongKeyLength:
	
	mov rdi, key_length_text
	call print_error_string
	.cont: call print_newline
	mov rax, 60
    xor rdi, rdi
    syscall