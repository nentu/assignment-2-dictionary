global find_word
section .data

%include "lib.inc"		;TODO
section .text


;rdi- ссылка на первый элемент
;rsi - ссылка на искомую строку

;next label
;+8 - label key
;+16 - label value
find_word:
	mov rax, rdi
	.main_loop:
	cmp [rax], word 0x0
	je .end_loop
	mov rdi, [rax+8]
	;add rdi, 24	;key string label
	push rax
	call string_equals
	test rax, rax
	pop rax
	je .cont
	mov rax, [rax+16]
	ret
	.cont: mov rax, [rax]
	jmp .main_loop
	.end_loop: xor rax, rax
	ret	
	