global string_copy
global parse_int
global parse_uint
global read_word
global read_char
global string_equals
global print_int
global print_uint
global print_char
global print_string
global string_length
global exit
global print_newline
global read_string
global print_error_string
global print_error_char

section .data
	new_char: db 0x0
section .text
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	push rbx;
	.loop: mov rbx, [rdi+rax]
	cmp bl, byte 0x0
	jne .cont
	pop rbx;
    ret
	.cont: inc rax
	jmp .loop


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
	mov     rdx, rax          ; string length in bytes
    mov     rax, 1           ; 'write' syscall number
	mov     rsi, rdi     ; string address
    mov     rdi, 1           ; stdout descriptor
    syscall
	ret
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	
	mov rax, 1 ; 'write' syscall identifier
	mov rdi, 1 ; stdout file descriptor
	mov rsi, rsp ; where do we take data from
	mov rdx, 1 ; the amount of bytes to write
	syscall

	pop rdi
	ret

print_error_char:
	push rdi
	
	mov rax, 1 ; 'write' syscall identifier
	mov rdi, 2 ; stdout file descriptor
	mov rsi, rsp ; where do we take data from
	mov rdx, 1 ; the amount of bytes to write
	syscall

	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)

print_newline:
	mov rdi, 10
	call print_char
	ret

; Выводит беззнаковое rdi:8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
	push rbx
	mov rsi, rsp
	xor rdx, rdx
	push rdx
	
	
	mov rax, rdi   ; Делимое в регистр AX
	.readLoop:
	xor rdx, rdx
	mov rbx, 10   ; Делитель в регистр BL
	div rbx        ; Теперь АL = 250 / 150 = 1, AH = 100
	add rdx, '0'
	push rdx	
	
	test rax, rax
	jne .readLoop
	
	.printLoop:
	pop rdi
	test rdi, rdi
	je .end
	call print_char
	jmp .printLoop
	.end:
	pop rbx
    ret

; Выводит знаковое rdi: 8-байтовое число в десятичном формате 

print_int:
    xor rax, rax
	cmp rdi, 0
	jge .plus
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.plus:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
	mov rax, 1
	xor rcx, rcx
	push rbx;
	push rdx;
	push rcx;
	
	loopp2: mov rbx, [rdi+rcx]
	mov rdx, [rsi+rcx]
	and rbx, 0xff		
	and rdx, 0xff
	cmp rbx, rdx
	je cont2
	mov rax, 0
	cont2: test rbx, rbx
	jne contInc
	pop rcx
	pop rdx
	pop rbx
    ret
	contInc: inc rcx
	jmp loopp2


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

read_char:
    xor rax, rax
	mov rax, 0 ; 'read' syscall number
	mov rdi, 0 ; stdin descriptor
	mov rsi, new_char ; string address
	mov rdx, 1 ; string length in bytes
	syscall
	test rax, rax
	je .end
	mov rax, [new_char]
	
	.end:
    ret 

; Принимает: rdi: адрес начала буфера, rsi: размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_string:
	push rdi
	push rsi
	xor rdx, rdx
	mov rax, rdi
	push rax
	
	xor rax, rax
	add rsi, rdi
	
	.loopRead: push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp al, 0xA
	je .end
	.workWithChar:test al, al
	je .end
	
	
	cmp rsi, rdi
	jne .goodLength
	.badLength: 
	xor rdx, rdx
	pop rax
	xor rax, rax
	push rax
	jmp .end
	
	.goodLength:mov [rdi], rax
	inc rdi
	inc rdx
	jmp .loopRead
	.end:
	mov [rdi], byte 0x0
	
	pop rax
	pop rsi
	pop rdi
    ret
 
 read_word:
	push rdi
	push rsi
	xor rdx, rdx
	mov rax, rdi
	push rax
	
	xor rax, rax
	add rsi, rdi
	.loopSpaces: push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp al, 0x20
	je .loopSpaces
	cmp al, 0x9
	je .loopSpaces
	cmp al, 0xA
	je .loopSpaces
	jmp .workWithChar
	
	.loopRead: push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	 cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xA
	je .end
	.workWithChar:test al, al
	je .end
	
	
	cmp rsi, rdi
	jne .goodLength
	.badLength: 
	xor rdx, rdx
	pop rax
	xor rax, rax
	push rax
	jmp .end
	
	.goodLength:mov [rdi], rax
	inc rdi
	inc rdx
	jmp .loopRead
	.end:
	mov [rdi], byte 0x0
	
	pop rax
	pop rsi
	pop rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error_string:
	push rdi
	call string_length
	pop rdi
	mov     rdx, rax          ; string length in bytes
    mov     rax, 1           ; 'write' syscall number
	mov     rsi, rdi     ; string address
    mov     rdi, 2           ; stdout descriptor
    syscall
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
	xor rax, rax
	push rsi
	xor rdx, rdx
	xor rsi, rsi	;ans
	.mainLoop:
	mov rax, [rdi]
	inc rdi
	cmp al, '0'
	jb .end
	cmp al, '9'
	ja .end
	
	push rdx
	imul rsi, 10
	pop rdx
	jc .badValue
	
	
	sub al, 0x30
	add sil, al
	jc .badValue
	
	jnc .goodValue
	.badValue: xor rdx, rdx
	jmp .end
	
	.goodValue: inc rdx
	
	jmp .mainLoop
	.end:
	mov rax, rsi
	.finish:
	pop rsi
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	xor rdx, rdx
	cmp byte [rdi], '-'
	jne .plus
	
	.minus:
	mov rdx, 1
	inc rdi
	
	.plus:push rdx
	call parse_uint
	cmp rdx, 0
	je .badEnd ;Не прочиталось
	
	cmp rax, 0x0
	jb .badEnd
	
	cmp byte[rsp], 0x0
	je .end
	inc rdx
	neg rax
	jmp .end
	.badEnd: xor rdx, rdx
	.end: add rsp, 8
	ret 

; rdi: Принимает указатель на строку, rsi: указатель на буфер и rdx: длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
	push rdi
	push rsi
    call string_length
	inc rax
	pop rsi
	pop rdi
	cmp rax, rdx
	ja .badEnd
	push rbx
	push rax
	xor rax, rax
	.loop: mov rbx, [rdi+rax]
	mov [rsi+rax], rbx
	inc rax
	cmp rax, [rsp]
	jne .loop

	pop rax
	pop rbx
    ret
	.badEnd: xor rax, rax
	ret
	
