import unittest
import sys, subprocess

from dictManadger import DictManadger
unittest.TestLoader.sortTestMethodsUsing = None
tests = {
            "simple":{
                "key1": "value1", 
                "key2": "value2",
                "key3": "value3"
            },
            "with spaces":{
                "key 1": "value1 value1 value1", 
                "key 2": "value2 value2 value2",
                "key 3": "value3 value3 value3"
            },
            "long words":{
                "v"*255:"v"*500,
                "s"*255:"s",
                "c"*255:"c",
                "asdasdas ffwef":"effwfwef"
            },
            "strange symbols":{
                "UausdgIYFyasdu$!@#&^!#%! ))@(_)(@$)":"Y@*YEIHIDGU@T#@ 0)}\{\}_O}",
                ".............":"..."
            },
            "empty items":{
                "":"empty",
                "empty":"",
            },
            "full empty item":{
                "":""
            }
            
        }   

class TestSum(unittest.TestCase):
    def _full_dict_check(self, v: dict):
        dictM = DictManadger(v)
        for req, resp in v.items():
            self.assertEqual(dictM.get(req), (resp, ""), f"Should be '{resp}'")

    def test_simlpe_get(self):
        v = tests["simple"]
        dictM = DictManadger(v)
        for req, resp in v.items():
            dictResp = dictM.get(req)
            self.assertEqual((dictResp[0].split('\n')[-1], dictResp[1]), (resp, ""), f"Should be '{resp}'")

    
    def test_space_get(self):
        self._full_dict_check(tests["with spaces"])

    
    def test_long_key(self):
        self._full_dict_check(tests["long words"])

    def test_strange_key(self):
        self._full_dict_check(tests["strange symbols"])

    def test_empty_items(self):
        self._full_dict_check(tests["empty items"])

    def test_full_empty_item(self):
        self._full_dict_check(tests["full empty item"])
    
    def test_too_long_key(self):
        v = tests["long words"]
        dictM = DictManadger(v)
        key = "v"*256
        self.assertEqual(dictM.get(key), ("", "The key length must be less than 255."), f"Should be error 'The key length must be less than 255.'")
        key = ""

    def test_wrong_key(self):
        v = tests["simple"]
        dictM = DictManadger(v)
        key = "sddfsdf"
        self.assertEqual(dictM.get(key), ("", f"Key error: \"{key}\""), f"Should be error 'Key error: \"{key}\"'")
        key = ""
        self.assertEqual(dictM.get(key), ("", f"Key error: \"{key}\""), f"Should be error 'Key error: \"{key}\"'")



if __name__ == '__main__':
    unittest.main()





# for k, v in tests.items():
#     print(k)
#     compile_main(v)
#     print(get_from_dict("first word"))
#     print(get_from_dict(""))
#     break